extern crate melpabot;

use std::{env::args, process::exit};

fn main() {
    let query = match args().nth(1) {
        Some(query) => query,
        None => {
            eprintln!("Please provide a search string");
            exit(1)
        }
    };
    let archive = match melpabot::get_archive() {
        Ok(archive) => archive,
        Err(error) => {
            eprintln!("{}", format!("Error fetching melpa.org: {:?}", error));
            exit(2);
        }
    };
    let results = archive.search(&query);
    if results.len() == 0 {
        eprintln!(
            "{}",
            format!("Couldn’t find any package matching '{}'", query)
        );
        exit(3)
    }
    for entry in results {
        println!("{}", pretty(&entry))
    }
}

fn pretty(entry: &melpabot::MelpaEntry) -> String {
    let repository_url = match entry.repository_url {
        Some(ref url) => url.clone(),
        None => String::from("N/A"),
    };
    format!("{}\t{}\t{}", entry.name, repository_url, entry.description)
}
