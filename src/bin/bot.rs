use erased_serde::Serialize;
use futures::stream::Stream;
use futures::IntoFuture;
use std::env;
use telebot::functions::*;
use telebot::objects::*;
use telebot::RcBot;
use tokio_core::reactor::Core;

use melpabot;

const KEY: &str = "TELEGRAM_BOT_TOKEN";

fn main() {
    let bot_token = match env::var(KEY) {
        Ok(val) => val,
        Err(e) => {
            println!("couldn't get enviroment variablbe {}: {}", KEY, e);
            ::std::process::exit(1)
        }
    };

    // Create a new tokio core
    let mut lp = Core::new().unwrap();

    // Create the bot
    let bot = RcBot::new(lp.handle(), &bot_token).update_interval(200);

    let stream = bot
        .get_stream()
        .filter_map(|(bot, msg)| msg.inline_query.map(|query| (bot, query)))
        .and_then(|(bot, query)| {
            let package_results = inline_results(&query.query);
            bot.answer_inline_query(query.id, package_results)
                .is_personal(true)
                .send()
        });

    // enter the main loop
    lp.run(stream.for_each(|_| Ok(())).into_future()).unwrap();
}

fn inline_results(query: &str) -> Vec<Box<Serialize>> {
    use melpabot::MelpaError::*;
    let packages = match melpabot::get_archive() {
        Ok(packages) => packages,
        Err(error @ Parse) | Err(error @ Fetch) => {
            eprintln!("Error: {:?}", error);
            return vec![];
        }
    };
    packages
        .search(query)
        .iter()
        .map(|entry| {
            let inline_result_text = format!(
                "<strong>Package</strong>: {}</br><strong>Description</strong>: {}<strong>",
                &entry.name, &entry.description,
            );
            let inline_result_item = input_message_content::Text::new(inline_result_text)
                .parse_mode("html")
                .disable_web_page_preview(true);

            let mut result_buttons_row = Vec::new();
            result_buttons_row.push(
                InlineKeyboardButton::new("Melpa URL".to_string()).url(entry.melpa_url.clone()),
            );
            if let Some(ref repository_url) = &entry.repository_url {
                result_buttons_row.push(
                    InlineKeyboardButton::new("Repository".to_string())
                        .url(repository_url.to_string()),
                );
            }
            let result_buttons = InlineKeyboardMarkup {
                inline_keyboard: vec![result_buttons_row],
            };

            let inline_entry =
                InlineQueryResultArticle::new(entry.name.clone(), Box::new(inline_result_item))
                    .reply_markup(result_buttons);

            Box::new(inline_entry.description(entry.description.clone())) as Box<Serialize>
        })
        .collect()
}
