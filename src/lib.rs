use reqwest;
use serde_derive::Deserialize;
use std::collections::HashMap;

const ARCHIVE_URL: &str = "https://melpa.org/archive.json";
const BASE_URL: &str = "https://melpa.org/#/";

#[derive(Debug)]
pub enum MelpaError {
    Fetch,
    Parse,
}

#[derive(Debug, PartialEq)]
pub struct MelpaEntry {
    pub name: String,
    pub version: (u32, u32),
    pub description: String,
    pub repository_url: Option<String>,
    pub melpa_url: String,
}

#[derive(Debug)]
pub struct MelpaEntries(Vec<MelpaEntry>);

impl MelpaEntries {
    pub fn search(&self, text: &str) -> Vec<(&MelpaEntry)> {
        self.0
            .iter()
            .filter(|e| e.name.contains(text) || e.description.contains(text))
            .collect()
    }
}

/// Tries to send a GET request to Melpa and deserializes the response into a `MelpaEntries` struct.
pub fn get_archive() -> Result<MelpaEntries, MelpaError> {
    reqwest::get(ARCHIVE_URL)
        .or(Err(MelpaError::Fetch))
        .and_then(|mut response| {
            response
                .json()
                .or(Err(MelpaError::Parse))
                .and_then(|json: MelpaJsonArchive| Ok(json.into()))
        })
}

type MelpaJsonArchive = HashMap<String, JsonArchiveEntry>;

impl From<MelpaJsonArchive> for MelpaEntries {
    fn from(json_object: MelpaJsonArchive) -> MelpaEntries {
        let mut json_object = json_object;
        let mut entries = Vec::new();
        for (name, data) in json_object.drain() {
            let entry = MelpaEntry {
                melpa_url: format!("{}{}", BASE_URL, &name),
                name: name,
                version: data.ver,
                description: data.desc,
                repository_url: data.props.and_then(|p| p.url),
            };
            entries.push(entry);
        }
        MelpaEntries(entries)
    }
}

#[derive(Deserialize, Debug)]
/// Used to extract values from the `archive.json` melpa file.
struct JsonArchiveEntry {
    ver: (u32, u32),
    desc: String,
    props: Option<Props>,
}

#[derive(Deserialize, Debug)]
struct Props {
    url: Option<String>,
}

#[cfg(test)]
mod test {
    use super::*;

    fn deserialize_sample() -> MelpaJsonArchive {
        use std::fs::read_to_string;
        let serialized = read_to_string("tests/sample-archive.json").unwrap();
        let deserialized: MelpaJsonArchive = serde_json::from_str(&serialized).unwrap();
        deserialized
    }

    #[test]
    fn deserialize_archive() {
        deserialize_sample();
    }

    #[test]
    fn from() {
        let deserialized = deserialize_sample();
        let _entries: MelpaEntries = deserialized.into();
    }

    #[test]
    fn search() {
        let entries: MelpaEntries = deserialize_sample().into();
        let search_results = entries.search("rust-mode");

        let rust_mode = MelpaEntry {
            name: "rust-mode".into(),
            version: (20190125, 2135),
            description: "A major emacs mode for editing Rust source code".into(),
            repository_url: Some("https://github.com/rust-lang/rust-mode".into()),
            melpa_url: "https://melpa.org/#/rust-mode".into(),
        };
        let flymake_rust = MelpaEntry {
            name: "flymake-rust".into(),
            version: (20170729, 2139),
            description: "A flymake handler for rust-mode files".into(),
            repository_url: Some("https://github.com/joaoxsouls/flymake-rust".into()),
            melpa_url: "https://melpa.org/#/flymake-rust".into(),
        };

        assert!(search_results.contains(&&rust_mode));
        assert!(search_results.contains(&&flymake_rust));
    }
}
